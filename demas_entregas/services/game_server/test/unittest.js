var expect = require("chai").expect;
const { moveChar, olderState } = require("../src/sokoban.js");

describe("Sokoban unit test", function () {
  describe("#move()", function () {
    it("Should move pos", function () {
      let currentState = {
        level: [
          [1, 1, 1, 1, 1],
          [1, 2, 2, 3, 1],
          [1, 1, 1, 1, 1],
        ],
        complete: false,
        pos: [1, 1],
        box: [1, 2],
        moves: 0,
        push: 0,
      };
      let expectedPos = [1, 2];
      expect(moveChar(currentState, 1).pos).to.eql(expectedPos);
    });
    it("Should not move (Char collides with wall)", function () {
      let currentState = {
        level: [
          [1, 1, 1, 1, 1],
          [1, 2, 2, 3, 1],
          [1, 1, 1, 1, 1],
        ],
        complete: false,
        pos: [1, 1],
        box: [1, 3],
        moves: 0,
        push: 0,
      };
      let expectedPos = [1, 1];
      expect(moveChar(currentState, 0).pos).to.eql(expectedPos);
    });
    it("Should not move (Box collides with second box)", function () {
      let currentState = {
        level: [
          [1, 1, 1, 1, 1],
          [1, 2, 2, 3, 1],
          [1, 1, 1, 1, 1],
        ],
        complete: false,
        pos: [1, 1],
        box: [1, 2, 1, 3],
        moves: 0,
        push: 0,
      };
      let expectedPos = [1, 1];
      expect(moveChar(currentState, 1).pos).to.eql(expectedPos);
    });
    it("Should not move (Box collides with wall)", function () {
      let currentState = {
        level: [
          [1, 1, 1, 1, 1],
          [1, 2, 2, 3, 1],
          [1, 1, 1, 1, 1],
        ],
        complete: true,
        pos: [1, 2],
        box: [1, 3],
        moves: 0,
        push: 0,
      };
      let expectedState = {
        level: [
          [1, 1, 1, 1, 1],
          [1, 2, 2, 3, 1],
          [1, 1, 1, 1, 1],
        ],
        complete: true,
        pos: [1, 2],
        box: [1, 3],
        moves: 0,
        push: 0,
      };
      expect(moveChar(currentState, 1)).to.eql(expectedState);
    });
    it("Should move, push the box and complete the level", function () {
      let currentState = {
        level: [
          [1, 1, 1, 1, 1],
          [1, 2, 2, 3, 1],
          [1, 1, 1, 1, 1],
        ],
        complete: false,
        pos: [1, 1],
        box: [1, 2],
        moves: 0,
        push: 0,
      };
      let expectedState = {
        level: [
          [1, 1, 1, 1, 1],
          [1, 2, 2, 3, 1],
          [1, 1, 1, 1, 1],
        ],
        complete: true,
        pos: [1, 2],
        box: [1, 3],
        moves: 1,
        push: 1,
      };
      expect(moveChar(currentState, 1)).to.eql(expectedState);
    });
  });
  describe("#olderState()", function () {
    it("Should return the second state", function () {
      let listmoves = [1, 1, 0, 2, 3, 1, 1];
      let initialState = {
        level: [
          [1, 1, 1, 1, 1, 1],
          [1, 2, 2, 2, 3, 1],
          [1, 1, 1, 1, 1, 1],
        ],
        complete: false,
        pos: [1, 1],
        box: [1, 4],
        moves: 0,
        push: 0,
      };
      let expectedState = {
        level: [
          [1, 1, 1, 1, 1, 1],
          [1, 2, 2, 2, 3, 1],
          [1, 1, 1, 1, 1, 1],
        ],
        complete: true,
        pos: [1, 3],
        box: [1, 4],
        moves: 2,
        push: 0,
      };
      expect(olderState(initialState, listmoves, 2)).to.eql(expectedState);
    });
    it("Should get to final state", function () {
      let listmoves = [1, 1, 1, 1, 3, 3, 0, 1, 1, 3, 3, 2, 2, 1, 1];
      let initialState = {
        level: [
          [1, 1, 1, 1, 1, 1, 1, 1],
          [1, 2, 2, 2, 2, 2, 3, 1],
          [1, 2, 2, 2, 2, 2, 3, 1],
          [1, 2, 2, 2, 2, 2, 3, 1],
          [1, 1, 1, 1, 1, 1, 1, 1],
        ],
        complete: false,
        pos: [2, 1],
        box: [1, 4, 2, 4, 3, 4],
        moves: 0,
        push: 0,
      };
      let expectedState = {
        level: [
          [1, 1, 1, 1, 1, 1, 1, 1],
          [1, 2, 2, 2, 2, 2, 3, 1],
          [1, 2, 2, 2, 2, 2, 3, 1],
          [1, 2, 2, 2, 2, 2, 3, 1],
          [1, 1, 1, 1, 1, 1, 1, 1],
        ],
        complete: true,
        pos: [3, 5],
        box: [1, 6, 2, 6, 3, 6],
        moves: 15,
        push: 6,
      };
      expect(olderState(initialState, listmoves, listmoves.length)).to.eql(
        expectedState
      );
    });
  });
});
