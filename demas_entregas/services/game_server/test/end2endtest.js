var expect = require("chai").expect;
const fetch = require("node-fetch");

//const BASE_URL = "https://sokoban-game-server.glitch.me/";
const BASE_URL = "http://localhost:3001";
const retry = 3;
const leveldata = JSON.stringify({
  listmoves: [1, 2, 3, 1],
  gamestate: {
    level: [
      [1, 1, 1, 1, 1],
      [1, 2, 2, 3, 1],
      [1, 1, 1, 1, 1],
    ],
    complete: false,
    pos: [1, 1],
    box: [1, 2],
    moves: 0,
    push: 0,
  },
  move: 3,
});

const fetchPost = (action, body, i) =>
  fetch(BASE_URL + action, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: body,
  }).then((response) => {
    if (response.ok) {
      return response.json();
    }
    if (i != 0) {
      return fetchPost(action, body, i - 1);
    }
    throw "Fetch put error";
  });

describe("Sokoban end to end test", function () {
  describe("#move()", function () {
    it("Should apply last move", async function () {
      let currentState,
        expectedState = {
          level: [
            [1, 1, 1, 1, 1],
            [1, 2, 2, 3, 1],
            [1, 1, 1, 1, 1],
          ],
          complete: true,
          pos: [1, 2],
          box: [1, 3],
          moves: 1,
          push: 1,
        };
      await fetchPost("/move", leveldata, retry).then(
        (r) => (currentState = r)
      );
      expect(currentState).to.eql(expectedState);
    });
  });
  describe("#back()", function () {
    it("Should return the third state", async function () {
      expectedState = {
        level: [
          [1, 1, 1, 1, 1],
          [1, 2, 2, 3, 1],
          [1, 1, 1, 1, 1],
        ],
        complete: true,
        pos: [1, 1],
        box: [1, 3],
        moves: 2,
        push: 1,
      };

      await fetchPost("/back", leveldata, retry).then(
        (r) => (currentState = r)
      );
      expect(currentState).to.eql(expectedState);
    });
  });
});
