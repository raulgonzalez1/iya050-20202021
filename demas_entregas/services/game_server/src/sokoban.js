const checkWin = (state) => {
  let aux = true;
  for (let i = 0; i < state.box.length / 2; i++) {
    if (state.level[state.box[i * 2]][state.box[i * 2 + 1]] != 3) {
      aux = false;
      break;
    }
  }
  state.complete = aux;
  return state;
};

const checkValidMove = (state, pos) => {
  if (state.level[pos[0]][pos[1]] === 1) {
    return false;
  }
  return true;
};

const checkBox = (state, pos, x, y) => {
  let aux = true;
  for (let i = 0; i < state.box.length / 2; i++) {
    if (pos[0] === state.box[i * 2] && pos[1] === state.box[i * 2 + 1]) {
      state = moveBox(state, [i * 2, i * 2 + 1], x, y);
      aux = false;
      break;
    }
  }
  if (aux) {
    state.pos = pos;
    state.moves += 1;
  }
  return state;
};

const move = (state, x, y) => {
  let newpos = [state.pos[0] + x, state.pos[1] + y];
  if (checkValidMove(state, newpos)) {
    state = checkBox(state, newpos, x, y);
  }
  return state;
};

const moveBox = (state, pos, x, y) => {
  let aux = true;
  if (state.level[state.box[pos[0]] + x][state.box[pos[1]] + y] != 1) {
    for (let i = 0; i < state.box.length / 2; i++) {
      if (
        state.box[pos[0]] + x === state.box[i * 2] &&
        state.box[pos[1]] + y === state.box[i * 2 + 1]
      ) {
        aux = false;
      }
    }
  } else {
    aux = false;
  }
  if (aux) {
    state.pos = [state.box[pos[0]], state.box[pos[1]]];
    state.box[pos[0]] = state.box[pos[0]] + x;
    state.box[pos[1]] = state.box[pos[1]] + y;
    state.moves += 1;
    state.push += 1;
  }
  return state;
};

const moveChar = (state, movement) => {
  switch (movement) {
    case 0:
      state = move(state, -1, 0);
      break;
    case 1:
      state = move(state, 0, 1);
      break;
    case 2:
      state = move(state, 1, 0);
      break;
    case 3:
      state = move(state, 0, -1);
      break;
  }
  state = checkWin(state);
  return state;
};

const olderState = (initialState, listmoves, step) => {
  return listmoves.slice(0, step).reduce(moveChar, initialState);
};

module.exports.moveChar = moveChar;
module.exports.olderState = olderState;
