const express = require("express");
const bodyParser = require("body-parser");
const { moveChar, olderState } = require("./sokoban.js");

const app = express();

app.use(bodyParser.json());

app.post("/move", function (req, res) {
  let body = req.body;
  body.gamestate = moveChar(
    body.gamestate,
    body.listmoves[body.listmoves.length - 1]
  );
  res.send(body.gamestate);
});

app.post("/back", function (req, res) {
  let body = req.body;
  body.gamestate = olderState(body.gamestate, body.listmoves, body.move);
  res.send(body.gamestate);
});

app.listen(process.env.PORT, () => {
  console.log("Servidor de juego");
});
