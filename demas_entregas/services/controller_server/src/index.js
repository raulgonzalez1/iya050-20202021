const Koa = require("koa");
const cors = require("@koa/cors");
const fetch = require("node-fetch");
const koaBody = require("koa-body");
const Router = require("@koa/router");
const levels = require("./levels.json");

const app = new Koa();
const router = new Router();

const GRAPHQL_URL = "http://stats_server:" + process.env.GRAPHQL;
const GAME_URL = "http://game_server:" + process.env.GAME;

const fetchQuery = (query) =>
  fetch(GRAPHQL_URL, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      query: query,
    }),
  }).then((res) => res.json());

const fetchGame = (action, data) =>
  fetch(GAME_URL + action, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: data,
  }).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw "Fetch put error";
  });

//ENVIAR EN UN HEADER "name" EL NOMBRE DEL USUARIO

router.post("/game", async (ctx) => {
  let body = ctx.request.body;
  let game = {};
  //Create new game
  Object.assign(game, levels[body.data]);
  //game = levels[body.data];
  game["levelNumber"] = body.data;
  game["push"] = 0;
  game["moves"] = 0;
  game["pos"] = game.start;
  game["complete"] = false;
  listmoves = [];
  let id;
  //Post to stat server
  await fetchQuery(
    `mutation{createPartida(name:"${ctx.request.header.name}",level:${body.data},Pos:[${game["pos"]}],Box:[${game["box"]}])}`
  ).then((e) => (id = e.data.createPartida));
  //Response to web
  ctx.response.set("Content-Type", "application/json");
  ctx.body = JSON.stringify({
    gamestate: game,
    listmoves,
    id,
  });
});

router.get("/game/:id", async (ctx) => {
  //Request current state
  let listMoves;
  let game = {};

  await fetchQuery(
    `query{partida(id:"${ctx.params.id}",name:"${ctx.request.header.name}"){level,nMoves,nPushes,playerPos,boxPos,listMoves,complete}}`
  )
    .then((e) => e.data.partida)
    .then((e) => {
      listMoves = e.listMoves;
      Object.assign(game, levels[e.level]);
      //game = levels[e.level];
      game["levelNumber"] = e.level;
      game["push"] = e.nPushes;
      game["moves"] = e.nMoves;
      game["pos"] = e.playerPos;
      game["box"] = e.boxPos;
      game["complete"] = e.complete;
    });
  //Response to web
  ctx.response.set("Content-Type", "application/json");
  ctx.body = JSON.stringify({
    id: ctx.params.id,
    game,
    listMoves,
  });
});

router.post("/game/:id/event", async (ctx) => {
  // 1. get current state from Stats server
  let listMoves;
  let game = {};
  let event = ctx.request.body.data.event;
  let move = ctx.request.body.data.move;

  await fetchQuery(
    `query{partida(id:"${ctx.params.id}",name:"${ctx.request.header.name}"){level,nMoves,nPushes,playerPos,boxPos,listMoves,complete}}`
  )
    .then((e) => e.data.partida)
    .then((e) => {
      if (e.listMoves != null) {
        listMoves = e.listMoves;
      }
      Object.assign(game, levels[e.level]);
      //game = levels[e.level];
      game["levelNumber"] = e.level;
      game["push"] = e.nPushes;
      game["moves"] = e.nMoves;
      game["pos"] = e.playerPos;
      game["box"] = e.boxPos;
      game["complete"] = e.complete;
    });

  if (event == "move" && game.complete == false) {
    listMoves.push(move);
  }

  body = JSON.stringify({
    gamestate: game,
    listmoves: listMoves,
    move,
  });

  // 2. get next state from Game server
  let newgamestate = game;
  if (newgamestate.complete == false) {
    await fetchGame(`/${event}`, body).then((e) => {
      newgamestate = e;
    });
  }

  // 3. post mutation to Stats server to store next state
  if (newgamestate.complete == false) {
    await fetchQuery(
      `mutation{updatePartida(name:"${ctx.request.header.name}",id:"${ctx.params.id}",level:${newgamestate.levelNumber},moves:${newgamestate.moves},push:${newgamestate.push},Pos:[${newgamestate.pos}],Box:[${newgamestate.box}],listMoves:[${listMoves}],complete:${newgamestate.complete})}`
    );
  } else {
    await fetchQuery(
      `mutation{completeLevel(name:"${ctx.request.header.name}",id:"${ctx.params.id}",level:${newgamestate.levelNumber},moves:${newgamestate.moves},push:${newgamestate.push},Pos:[${newgamestate.pos}],Box:[${newgamestate.box}],listMoves:[${listMoves}])}`
    );
  }

  // 4. return state
  ctx.response.set("Content-Type", "application/json");
  ctx.body = JSON.stringify({
    id: ctx.params.id,
    gamestate: newgamestate,
    listmoves: listMoves,
  });
});

app.use(koaBody());
app.use(cors());
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(process.env.PORT);
console.log("Servidor controlador");
