import React, { useState, useContext } from "react";
import { Redirect } from "react-router-dom";
import PropTypes from "prop-types";
import style from "./../style.css";
import TextInput from "./TextInput.jsx";
import AuthContext from "../AuthContext.js";
import AppContext from "../AppContext.js";
import fetchQuery from "../queryfetch.js";

const SignIn = ({ returnTo }) => {
  const { logIn, isLoggedIn } = useContext(AuthContext);
  const { change, changeR, changeH } = useContext(AppContext);

  const [formValues, setFormValues] = useState({ name: "" });

  if (isLoggedIn) return <Redirect to={returnTo} />;

  const handleInputChange = (event) => {
    const target = event.target;
    setFormValues({ ...formValues, [target.name]: target.value });
  };

  const logPlayerData = (name) => {
    //Player Data
    fetchQuery(
      `query{player(name:"${name}"){name levels{level complete recordMoves recordPushes}}}`
    ).then((e) => {
      if (!e.data.player) {
        fetchQuery(`mutation{createPlayer(name:"${formValues.name}")}`).then(
          (e) => logPlayerData(name)
        );
      } else {
        change(e);
      }
    });
    fetchQuery(
      `query{partidas(name:"${name}"){id level nMoves nPushes playerPos boxPos listMoves complete}}`
    ).then((e) => changeH(e));
  };

  const logRecordData = () => {
    //Record Data
    fetchQuery(`query{record{levels{level topPlayers{name moves}}}}`).then(
      (e) => {
        if (!e.data.record) {
          fetchQuery(`mutation{createRecord}`).then((e) => logRecordData());
        } else {
          changeR(e);
        }
      }
    );
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    logPlayerData(formValues.name);
    logRecordData();
    logIn(formValues);
  };

  return (
    <form onSubmit={handleSubmit} className={style.screencenter}>
      <TextInput
        label="Name"
        name="name"
        value={formValues.name}
        onChange={handleInputChange}
      />
      <div className={style.flexright}>
        <button type="submit">Submit</button>
      </div>
    </form>
  );
};
SignIn.propTypes = {
  returnTo: PropTypes.string.isRequired,
};

export default SignIn;
