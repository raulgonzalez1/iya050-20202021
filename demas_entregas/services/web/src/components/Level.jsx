import React, { useEffect, useContext, useState } from "react";
import AppContext from "../AppContext.js";
import AuthContext from "../AuthContext.js";
import { fetchQuery, fetchPost } from "../queryfetch.js";
import Square from "./Square.jsx";
import Entity from "./Entity.jsx";
import styles from "../gameStyle.css";

function Level(props) {
  const { user } = useContext(AuthContext);
  const [level, setLevel] = useState(props.level);
  const [id, setId] = useState(props.id);

  useEffect(() => {
    window.addEventListener("keydown", (event) => {
      keyToMove(event.key.toLocaleLowerCase());
    });
    return () => {
      window.removeEventListener("keydown", (event) => {
        keyToMove(event.key.toLocaleLowerCase());
      });
    };
  }, []);

  const keyToMove = (e) => {
    switch (e.toLocaleLowerCase()) {
      case "w":
        move(0);
        break;
      case "s":
        move(2);
        break;
      case "a":
        move(3);
        break;
      case "d":
        move(1);
        break;
    }
  };

  const move = (number) => {
    fetchPost(
      `game/${id}/event`,
      { move: number, event: "move" },
      user.name
    ).then((data) => setLevel(data));
  };

  const resetLevel = () => {
    fetchPost(`game/${id}/event`, { move: 1, event: "back" }, user.name).then(
      (data) => setLevel(data)
    );
  };

  const drawGame = () => {
    let content = [];
    let game = [];
    //Add level tiles
    for (let i = 0; i < level.gamestate.level.length; i++) {
      content = [];
      for (let j = 0; j < level.gamestate.level[i].length; j++) {
        content.push(
          <Square value={level.gamestate.level[i][j]} key={i + "-" + j} />
        );
      }
      game.push(
        <div className={styles.row} key={i}>
          {content}
        </div>
      );
    }
    //Add player and boxes
    game.push(
      <Entity
        value={"player"}
        posx={level.gamestate.pos[0]}
        posy={level.gamestate.pos[1]}
        key={"player"}
      />
    );
    for (let i = 0; i < level.gamestate.box.length / 2; i++) {
      if (
        level.gamestate.level[level.gamestate.box[i * 2]][
          level.gamestate.box[i * 2 + 1]
        ] == 3
      ) {
        game.push(
          <Entity
            value={"boxgoal"}
            posx={level.gamestate.box[i * 2]}
            posy={level.gamestate.box[i * 2 + 1]}
            key={"box - " + i}
          />
        );
      } else {
        game.push(
          <Entity
            value={"box"}
            posx={level.gamestate.box[i * 2]}
            posy={level.gamestate.box[i * 2 + 1]}
            key={"box - " + i}
          />
        );
      }
    }
    //Add level complete message
    var complete = "";
    if (level.gamestate.complete) {
      complete = "LEVEL COMPLETE";
    }
    game.push(
      <div className={styles.controls} key="complete">
        <div className={styles.reset} onClick={() => resetLevel()}>
          <svg viewBox="0 0 512 512">
            <path
              transform="scale(-1, 1) translate(-500, 0)"
              d="M256.455 8c66.269.119 126.437 26.233 170.859 68.685l35.715-35.715C478.149 25.851 504 36.559 504 57.941V192c0 13.255-10.745 24-24 24H345.941c-21.382 0-32.09-25.851-16.971-40.971l41.75-41.75c-30.864-28.899-70.801-44.907-113.23-45.273-92.398-.798-170.283 73.977-169.484 169.442C88.764 348.009 162.184 424 256 424c41.127 0 79.997-14.678 110.629-41.556 4.743-4.161 11.906-3.908 16.368.553l39.662 39.662c4.872 4.872 4.631 12.815-.482 17.433C378.202 479.813 319.926 504 256 504 119.034 504 8.001 392.967 8 256.002 7.999 119.193 119.646 7.755 256.455 8z"
            />
          </svg>
        </div>
        <div className={styles.complete}>{complete}</div>
      </div>
    );
    return game;
  };

  var levelData = drawGame();

  return <div className={styles.gameboard}>{levelData}</div>;
}

export default Level;
