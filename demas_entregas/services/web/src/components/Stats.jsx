import React, { useContext, useState } from "react";
import AuthContext from "../AuthContext.js";
import AppContext from "../AppContext.js";
import style from "./../style.css";

const Stats = () => {
  const { user } = useContext(AuthContext);
  var stats = useContext(AppContext).stats;
  var record = useContext(AppContext).record;

  const [{ isLevel, level }, setIsLevel] = useState({
    isLevel: false,
    level: 0,
  });

  const complete = (bool) => {
    if (bool) {
      return { backgroundColor: "green" };
    }
    return { backgroundColor: "red" };
  };

  const fillLevelTable = (i) => {
    if (record != undefined && i != undefined) {
      let j = 0;
      return record.data.record.levels[i].topPlayers.map((record) => {
        return (
          <tr key={j++}>
            <td>{record.name}</td>
            <td>{record.moves}</td>
          </tr>
        );
      });
    }
  };

  const fillTable = () => {
    if (stats != undefined) {
      var i = 0;
      return stats.data.player.levels.map((level) => {
        return (
          <tr onClick={() => displayLevel(level.level)} key={i++}>
            <td>{level.level}</td>
            <td style={complete(level.complete)}></td>
            <td>{level.recordMoves}</td>
            <td>{level.recordPushes}</td>
          </tr>
        );
      });
    }
  };

  const displayLevel = (i) => {
    setIsLevel({ isLevel: true, level: i });
  };

  const fill = () => {
    if (isLevel) {
      return (
        <>
          <button onClick={() => setIsLevel({ isLevel: false })}>
            Go back
          </button>
          <table>
            <thead>
              <tr>
                <th>Name</th>
                <th>Moves</th>
              </tr>
            </thead>
            <tbody>{levelTable}</tbody>
          </table>
        </>
      );
    } else {
      return (
        <>
          <p>Click on a level to view the top 10</p>
          <table>
            <thead>
              <tr>
                <th>Level</th>
                <th>Completed</th>
                <th>Record Moves</th>
                <th>Record Pushes</th>
              </tr>
            </thead>
            <tbody>{table}</tbody>
          </table>
        </>
      );
    }
  };

  var levelTable = fillLevelTable(level);
  var table = fillTable();
  var content = fill();

  return <div className={style.screencenter}>{content}</div>;
};

export default Stats;
