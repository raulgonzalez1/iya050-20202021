import React from "react";
import { Link } from "react-router-dom";
import style from "./../style.css";

const Landing = () => {
  return (
    <div className={style.screencenter}>
      <h1>Sokoban</h1>
      <div className={style.flexright}>
        <Link to="/sign-in">
          <button>Log in</button>
        </Link>
      </div>
    </div>
  );
};

export default Landing;
