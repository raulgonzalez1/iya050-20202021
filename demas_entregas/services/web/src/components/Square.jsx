import React from "react";
import styles from "../square.css";

//Componente base del tablero, comprueba el tipo para renderizar la imagen adecuada
class Square extends React.Component {
  constructor(props) {
    super(props);
  }

  squareType() {
    let typenumber = this.props.value;
    let type = "";
    switch (typenumber) {
      case 1:
        type = styles.wall;
        break;
      case 2:
        type = styles.floor;
        break;
      case 3:
        type = styles.goal;
    }
    return type;
  }

  render() {
    const style = `${styles.square} ${styles.img} ${this.squareType()}`;

    return <div className={style}></div>;
  }
}

export default Square;
