import React, { useContext, useEffect } from "react";
import style from "./../style.css";
import GenerateGame from "./GenerateGame.jsx";
import AppContext from "../AppContext.js";

const Play = () => {
  var partida = useContext(AppContext).partida;
  const { changeP } = useContext(AppContext);

  var datos = partida;

  useEffect(() => {
    return () => {
      changeP(undefined);
    };
  }, []);

  const fillTable = () => {
    if (datos != undefined) {
      if (datos.data.partida == null) return;
      return (
        <>
          <thead>
            <tr>
              <td>Level</td>
              <td>Number of Moves</td>
              <td>Number of Pushes</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{datos.data.partida.level}</td>
              <td>{datos.data.partida.nMoves}</td>
              <td>{datos.data.partida.nPushes}</td>
            </tr>
          </tbody>
        </>
      );
    }
  };

  var table = fillTable();
  var content;

  if (datos != undefined) {
    if (datos.data.partida == null) {
      content = <GenerateGame />;
    } else {
      content = <table>{table}</table>;
    }
  } else {
    content = <GenerateGame />;
  }

  return <div className={style.screencenter}>{content}</div>;
};

export default Play;
