import React, { useEffect, useContext, useState } from "react";
import AppContext from "../AppContext.js";
import AuthContext from "../AuthContext.js";
import { fetchQuery, fetchPost } from "../queryfetch.js";
import Level from "./Level.jsx";

function GenerateGame() {
  const { user } = useContext(AuthContext);
  if (useContext(AppContext).stats != undefined) {
    var stats = useContext(AppContext).stats.data;
  }
  const { change, changeR } = useContext(AppContext);
  const [level, setLevel] = useState(undefined);
  const [isLevel, setIsLevel] = useState(false);
  const [id, setId] = useState(undefined);

  const handleClick = (level) => {
    fetchPost("game", level, user.name).then((data) => {
      setLevel(data);
      setId(data.id);
      setIsLevel(true);
    });
  };

  const fillTable = () => {
    if (isLevel) {
      return "";
    }
    var levels = [0, 1, 2, 3, 4, 5, 6, 7];
    let i = 0;
    return levels.map((n) => {
      return (
        <tr key={i++}>
          <td onClick={() => handleClick(n)}>
            {"Level "}
            {i}
          </td>
        </tr>
      );
    });
  };

  const levelP = () => {
    if (!isLevel) {
      return "";
    }
    if (level != undefined && id != undefined) {
      return (
        <>
          <Level level={level} id={id} />
          <button onClick={() => setIsLevel(false)}>Back</button>
        </>
      );
    } else return "";
  };

  var levelData = levelP();
  var levelsTable = fillTable();

  return (
    <>
      <div>{levelData}</div>
      <table>
        <tbody>{levelsTable}</tbody>
      </table>
    </>
  );
}

export default GenerateGame;
