import React from "react";
import styles from "../entity.css";

//Entidades del juego (Jugador y cajas)
class Entity extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    let x = this.props.posx * 40;
    let y = this.props.posy * 40;
    let classname = `${styles.square} ${styles.img} ${
      styles[this.props.value]
    }`; //this.props.value + " square img";
    return (
      <div
        className={classname}
        style={{ position: "absolute", top: x, left: y }}
      ></div>
    );
  }
}

export default Entity;
