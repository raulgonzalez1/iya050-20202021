import React, { useContext, useState } from "react";
import AppContext from "../AppContext.js";
import AuthContext from "../AuthContext.js";
import style from "./../style.css";
import fetchQuery from "../queryfetch.js";
import { Redirect } from "react-router-dom";

const History = () => {
  var history = useContext(AppContext).history;
  const { changeP, change, changeH, changeR } = useContext(AppContext);
  const { user } = useContext(AuthContext);
  const [{ redirect }, setRedirect] = useState({ redirect: false });

  const handleClick = (id, user) => {
    fetchQuery(
      `query{partida(id:"${id}",name:"${user}"){level nMoves nPushes}}`
    ).then((e) => {
      if (!e) {
      } else {
        changeP(e);
        setRedirect({ redirect: true });
      }
    });
  };

  const refresh = (name) => {
    //Player Data
    fetchQuery(
      `query{player(name:"${name}"){name levels{level complete recordMoves recordPushes}}}`
    ).then((e) => {
      if (!e.data.player) {
        fetchQuery(`mutation{createPlayer(name:"${formValues.name}")}`).then(
          (e) => logPlayerData(name)
        );
      } else {
        change(e);
      }
    });
    fetchQuery(
      `query{partidas(name:"${name}"){id level nMoves nPushes playerPos boxPos listMoves complete}}`
    ).then((e) => changeH(e));
    //Record Data
    fetchQuery(`query{record{levels{level topPlayers{name moves}}}}`).then(
      (e) => {
        if (!e.data.record) {
          fetchQuery(`mutation{createRecord}`).then((e) => logRecordData());
        } else {
          changeR(e);
        }
      }
    );
  };

  const fillTable = () => {
    if (history != undefined) {
      if (history.data.partidas != null) {
        var i = 0;
        return history.data.partidas.map((partida) => {
          return (
            <tr key={i++}>
              <td onClick={() => handleClick(partida.id, user.name)}>
                Partida {i} | nivel: {partida.level}
              </td>
            </tr>
          );
        });
      }
    }
    return "";
  };

  var table = fillTable();

  if (redirect) {
    return <Redirect to="/game/play" />;
  } else {
    return (
      <div className={style.screencenter}>
        <table>
          <thead>
            <tr>
              <th>Matches (Click to see stats)</th>
            </tr>
          </thead>
          <tbody>{table}</tbody>
        </table>
        <button onClick={() => refresh(user.name)}>Refresh</button>
        {redirect}
      </div>
    );
  }
};

export default History;
