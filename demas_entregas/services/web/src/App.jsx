import React, { useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Landing from "./components/Landing.jsx";
import Menu from "./components/Menu.jsx";
import SignIn from "./components/SignIn.jsx";
import AuthContext from "./AuthContext.js";
import { AppProvider } from "./AppContext.js";

const App = () => {
  const [{ isLoggedIn, user }, setAuth] = useState({
    isLoggedIn: false,
    user: undefined,
  });

  const [{ partida }, setPartida] = useState({
    partida: undefined,
  });
  const [{ record }, setRecord] = useState({
    record: undefined,
  });
  const [{ stats }, setStats] = useState({
    stats: undefined,
  });
  const [{ history }, setHistory] = useState({
    history: undefined,
  });

  return (
    <AuthContext.Provider
      value={{
        logIn: (user) => {
          setAuth({ isLoggedIn: true, user });
        },
        isLoggedIn,
        user,
      }}
    >
      <AppProvider
        value={{
          change: (stats) => {
            setStats({ stats });
          },
          changeP: (partida) => {
            setPartida({ partida });
          },
          changeR: (record) => {
            setRecord({ record });
          },
          changeH: (history) => {
            setHistory({ history });
          },
          partida,
          stats,
          record,
          history,
        }}
      >
        <Router>
          <Switch>
            <Route path="/" exact>
              <Landing />
            </Route>
            <Route path="/sign-in">
              <SignIn returnTo="/game" />
            </Route>
            <Route path="/game">
              <Menu />
            </Route>
            <Route path="/">
              <p>Page not found</p>
            </Route>
          </Switch>
        </Router>
      </AppProvider>
    </AuthContext.Provider>
  );
};

export default App;
