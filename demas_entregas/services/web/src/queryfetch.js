const url = "http://localhost:3000";
const controllerUrl = "http://localhost:3003/";

const fetchQuery = (query) =>
  fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      query: query,
    }),
  }).then((res) => res.json());

const fetchPost = (method, event, nombre) =>
  fetch(controllerUrl + method, {
    method: "POST",
    headers: { "Content-Type": "application/json", name: nombre },
    body: JSON.stringify({
      data: event,
    }),
  }).then((res) => res.json());

const fetchGet = (id) =>
  fetch(controllerUrl + "/game/" + id, {
    method: "GET",
    body: JSON.stringify({
      query: query,
    }),
  }).then((res) => res.json());

export { fetchGet, fetchPost };
export default fetchQuery;
