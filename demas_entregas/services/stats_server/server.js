const { GraphQLServer } = require("graphql-yoga");
const fetch = require("node-fetch");
const uuid = require("uuid");

const typeDefs = `
  type Query {
    player(name:String!): Player
    partidas(name:String!): [Partida]
    partida(id:String!, name:String!): Partida
    record: Record
  }
  type Mutation {
    createPlayer(name: String!): String
    createPartida(name:String! ,level:Int! ,Pos:[Int]! ,Box:[Int]!): String
    updatePartida(name:String!, id:String! ,level:Int, moves:Int, push:Int, Pos:[Int], Box:[Int], listMoves:[Int], complete: Boolean): String
    completeLevel(name:String!, id:String! ,level:Int, moves:Int, push:Int, Pos:[Int], Box:[Int], listMoves:[Int]): String
    createRecord: String
  }
  type Player {
    name: String!
    levels: [Level!]!
  }
  type Partida {
    id: String
    level: Int
    nMoves: Int
    nPushes: Int
    playerPos: [Int]
    boxPos: [Int]
    listMoves: [Int]
    complete: Boolean
  }
  type Level{
    level: Int
    complete: Boolean
    recordMoves: Int
    recordPushes: Int
  }
  type Record {
    levels: [Levelrecord]
  }
  type Levelrecord{
    level: String
    topPlayers: [Top]
  }
  type Top{
    name: String
    moves: Int
  }
`;

const retry = 3;
const BASE_URL =
  "https://0qh9zi3q9g.execute-api.eu-west-1.amazonaws.com/development";
const app_id = "30a00418-2316-4d2b-b73e-d6979a6c722d";

const createPartidaJson = (level, pos, box, id) => {
  return `
    {
      "id":"${id}",
      "level":${level},
      "nMoves":0,
      "nPushes":0,
      "playerPos": [${pos}],
      "boxPos": [${box}],
      "listMoves": [],
      "complete": false
    }
  `;
};

const createRecord = (n) => {
  let t = "";
  for (let i = 0; i < n; i++) {
    t += `{
      "level":${i},
      "topPlayers":[]
    }`;
    if (i != n - 1) {
      t += ",";
    }
  }
  return t;
};

const createLevel = (n) => {
  let t = "";
  for (let i = 0; i < n; i++) {
    t += `{
      "level":${i},
      "complete":false,
      "recordMoves":null,
      "recordPushes":null
    }`;
    if (i != n - 1) {
      t += ",";
    }
  }
  return t;
};

const fetchGet = (resource, i) =>
  fetch(BASE_URL + resource, {
    method: "GET",
    headers: {
      "x-application-id": app_id,
    },
  }).then((response) => {
    if (response.ok) {
      return response.json();
    }
    if (i != 0) {
      return fetchGet(resource, i - 1);
    }
    throw "Fetch get error";
  });

const fetchPut = (resource, query, i) =>
  fetch(BASE_URL + resource, {
    method: "PUT",
    headers: {
      "x-application-id": app_id,
      "Content-Type": "application/json",
    },
    body: query,
  }).then((response) => {
    if (response.ok) {
      return response.json();
    }
    if (i != 0) {
      return fetchPut(resource, query, i - 1);
    }
    throw "Fetch put error";
  });

const toLevelJson = (data) => {
  let json = [];
  for (let i = 0; i < data.length; i++) {
    json.push(JSON.parse(data[i].value));
  }
  return json;
};

const resolvers = {
  Query: {
    player: (_, { name }) =>
      fetchGet("/pairs/user-" + name, retry)
        .then((data) => data.value)
        .then((player) => JSON.parse(player)),
    partidas: (_, { name }) =>
      fetchGet("/collections/user-" + name, retry)
        .then((data) => toLevelJson(data))
        .then((e) => e),
    partida: (_, { name, id }) =>
      fetchGet("/pairs/user-" + name + "-p-" + id, retry)
        .then((data) => data.value)
        .then((partida) => JSON.parse(partida)),
    record: (_, args) =>
      fetchGet("/pairs/record", retry)
        .then((data) => data.value)
        .then((record) => JSON.parse(record)),
  },

  //----------Mutations----------
  Mutation: {
    createPlayer(_, { name }) {
      var levels = createLevel(10);
      var json = `{
        "name":"${name}",
        "levels":[
          ${levels}
        ]
      }`;
      fetchPut("/pairs/user-" + name, json, retry);
    },
    createPartida(_, { name, level, Pos, Box }) {
      let id = uuid.v4();
      partida = createPartidaJson(level, Pos, Box, id);
      fetchPut("/pairs/user-" + name + "-p-" + id, partida, retry);
      return id;
    },
    updatePartida(
      _,
      { name, id, level, moves, push, Pos, Box, listMoves, complete }
    ) {
      partida = `{"id":"${id}","level":${level},"nMoves":${moves},"nPushes":${push},"playerPos":[${Pos}],"boxPos":[${Box}],"listMoves":[${listMoves}],"complete":${complete}}`;
      fetchPut("/pairs/user-" + name + "-p-" + id, partida, retry);
      return id;
    },
    completeLevel(_, { name, id, level, moves, push, Pos, Box, listMoves }) {
      partida = `{"id":"${id}","level":${level},"nMoves":${moves},"nPushes":${push},"playerPos":[${Pos}],"boxPos":[${Box}],"listMoves":[${listMoves}],"complete":true}}`;
      fetchPut("/pairs/user-" + name + "-p-" + id, partida, retry);
      //Update User
      fetchGet("/pairs/user-" + name, retry)
        .then((data) => JSON.parse(data.value))
        .then((e) => {
          e.levels[level].complete = true;
          if (
            e.levels[level].recordMoves == null ||
            e.levels[level].recordPushes > push
          ) {
            e.levels[level].recordPushes = push;
          }
          if (
            e.levels[level].recordMoves == null ||
            e.levels[level].recordMoves > moves
          ) {
            e.levels[level].recordMoves = moves;
          }
          return e;
        })
        .then((json) => {
          fetchPut("/pairs/user-" + name, JSON.stringify(json), retry);
        });
      //Update records
      fetchGet("/pairs/record", retry)
        .then((data) => JSON.parse(data.value))
        .then((e) => {
          //Put level in top 10
          let i = 0;
          let pushed = false;
          if (e.levels[level].topPlayers.length == 0) {
            e.levels[level].topPlayers.push({ name: name, moves: moves });
          } else {
            e.levels[level].topPlayers.map((r) => {
              if (r.moves > moves && !pushed) {
                e.levels[level].topPlayers.splice(i, 0, {
                  name: name,
                  moves: moves,
                });
                pushed = true;
              }
              i++;
            });
            if (!pushed) {
              e.levels[level].topPlayers.push({ name: name, moves: moves });
            }
            if (e.levels[level].topPlayers.length > 10) {
              e.levels[level].topPlayers.length = 10;
            }
          }
          return e;
        })
        .then((json) => {
          fetchPut("/pairs/record", JSON.stringify(json), retry);
        });
      return id;
    },
    createRecord(_, args) {
      let json = `{
        "levels":[${createRecord(10)}]  
      }`;
      fetchPut("/pairs/record", json, retry);
    },
  },

  //----------------------------------------------------------
  Player: {
    name: (player) => player.name,
    levels: (player) => player.levels,
  },
  Partida: {
    id: (partida) => partida.id,
    level: (partida) => partida.level,
    nMoves: (partida) => partida.nMoves,
    nPushes: (partida) => partida.nPushes,
    playerPos: (partida) => partida.playerPos,
    boxPos: (partida) => partida.boxPos,
    listMoves: (partida) => partida.listMoves,
    complete: (partida) => partida.complete,
  },
  Level: {
    level: (level) => level.level,
    complete: (level) => level.complete,
    recordMoves: (level) => level.recordMoves,
    recordPushes: (level) => level.recordPushes,
  },
  Record: {
    levels: (e) => e.levels,
  },
  Levelrecord: {
    level: (e) => e.level,
    topPlayers: (e) => e.topPlayers,
  },
  Top: {
    name: (e) => e.name,
    moves: (e) => e.moves,
  },
};

const server = new GraphQLServer({ typeDefs, resolvers });

server.express.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, privatekey"
  );
  next();
});

const opts = {
  cors: {
    credentials: true,
    origin: ["*"],
  },
  port: process.env.PORT,
  playground: "/",
};

server.start(opts).then(console.log("Servidor Stats GraphQL"));
