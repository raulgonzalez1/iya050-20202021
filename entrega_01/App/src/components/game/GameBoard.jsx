import React from "react";
import Square from "./Square.jsx";
import levels from "../../assets/levels.json";
import Entity from "./Entity.jsx";

class GameBoard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      level: this.createGameArray(
        levels[this.props.level].size[0],
        levels[this.props.level].size[1]
      ),
      pos: levels[this.props.level].start,
      boxes: levels[this.props.level].box,
      name: this.props.level,
      complete: false,
      xDown: null,
      yDown: null,
      xUp: null,
      yUp: null,
    };
  }

  //Reiniciar valores de las entidades y estado del nivel
  resetLevel() {
    this.setState({
      pos: levels[this.props.level].start,
      boxes: levels[this.props.level].box,
      complete: false,
    });
  }

  //Comprobar cambios de nivel
  componentDidUpdate() {
    if (this.props.level != this.state.name) {
      this.setState({
        level: this.createGameArray(
          levels[this.props.level].size[0],
          levels[this.props.level].size[1]
        ),
        pos: levels[this.props.level].start,
        boxes: levels[this.props.level].box,
        name: this.props.level,
        complete: false,
      });
    }
  }

  //Añadir event listeners al construir el componente
  componentDidMount() {
    document.addEventListener("touchstart", (event) => {
      this.handleTouchStart(event);
    });
    document.addEventListener("touchmove", (event) => {
      this.handleTouchMove(event);
    });
    document.addEventListener("touchend", (event) => {
      this.handleTouchEnd(event);
    });
    window.addEventListener("keydown", (event) => {
      this.keyToMove(event.key.toLocaleLowerCase());
    });
  }

  componentWillUnmount() {
    document.removeEventListener("touchstart", (event) => {
      this.handleTouchStart(event);
    });
    document.removeEventListener("touchmove", (event) => {
      this.handleTouchMove(event);
    });
    document.removeEventListener("touchend", (event) => {
      this.handleTouchEnd(event);
    });
    window.removeEventListener("keydown", (event) => {
      this.keyToMove(event.key.toLocaleLowerCase());
    });
  }

  //TOUCH CONTROLS
  handleTouchStart(evt) {
    const firstTouch = evt.touches[0];
    this.setState({ xDown: firstTouch.clientX, yDown: firstTouch.clientY });
  }

  handleTouchMove(evt) {
    this.setState({ xUp: evt.touches[0].clientX, yUp: evt.touches[0].clientY });
  }

  handleTouchEnd(evt) {
    if (
      !this.state.xDown ||
      !this.state.yDown ||
      !this.state.xUp ||
      !this.state.yUp
    ) {
      return;
    }
    var xDiff = this.state.xDown - this.state.xUp;
    var yDiff = this.state.yDown - this.state.yUp;

    if (Math.abs(xDiff) > Math.abs(yDiff)) {
      if (xDiff > 0) {
        this.moveChar(0, -1);
      } else {
        this.moveChar(0, 1);
      }
    } else {
      if (yDiff > 0) {
        this.moveChar(-1, 0);
      } else {
        this.moveChar(1, 0);
      }
    }
    this.setState({
      xDown: null,
      yDown: null,
      xUp: null,
      yUp: null,
    });
  }

  //Crear array del nivel con los datos del tablero
  createGameArray(x, y) {
    let gamearray = new Array(x);
    for (let i = 0; i < gamearray.length; i++) {
      gamearray[i] = new Array(y);
      for (let j = 0; j < gamearray[i].length; j++) {
        gamearray[i][j] =
          levels[this.props.level].level[gamearray[i].length * i + j];
      }
    }
    return gamearray;
  }

  //Comprobar si la caja se puede mover(delante tiene una casilla valida y no existe otra caja)
  moveBox(i, x, y) {
    if (
      this.state.level[this.state.pos[0] + x * 2][this.state.pos[1] + y * 2] !=
      1
    ) {
      for (let i = 0; i < this.state.boxes.length / 2; i++) {
        if (
          this.state.boxes[i * 2] == this.state.pos[0] + x * 2 &&
          this.state.boxes[i * 2 + 1] == this.state.pos[1] + y * 2
        ) {
          return false;
        }
      }
      let newboxes = this.state.boxes.slice();
      newboxes[i] += x;
      newboxes[i + 1] += y;
      this.setState({ boxes: newboxes });
      return true;
    }
    return false;
  }

  //Comprobar si el movimiento que se intenta realizar es valido(delante no hay nada o si hay una caja esta se puede mover)
  checkValidMove(x, y) {
    if (this.state.level[this.state.pos[0] + x][this.state.pos[1] + y] != 1) {
      for (let i = 0; i < this.state.boxes.length / 2; i++) {
        if (
          this.state.boxes[i * 2] == this.state.pos[0] + x &&
          this.state.boxes[i * 2 + 1] == this.state.pos[1] + y
        ) {
          if (this.moveBox(i * 2, x, y)) {
            return true;
          }
          return false;
        }
      }
      return true;
    }
    return false;
  }

  //Comprobar si las cajas estan en las posiciones correctas
  checkWin() {
    for (let i = 0; i < this.state.boxes.length / 2; i++) {
      if (
        this.state.level[this.state.boxes[i * 2]][
          this.state.boxes[i * 2 + 1]
        ] != 3
      ) {
        return false;
      }
    }
    return true;
  }

  //Mover al jugador
  moveChar(x, y) {
    let newpos = [this.state.pos[0] + x, this.state.pos[1] + y];
    if (this.checkValidMove(x, y)) {
      this.setState({ pos: newpos });
      if (this.checkWin()) {
        this.setState({ complete: true });
      }
    }
  }

  //Transformar input en moviemiento del jugador
  keyToMove(e) {
    switch (e.toLocaleLowerCase()) {
      case "w":
        this.moveChar(-1, 0);
        break;
      case "s":
        this.moveChar(1, 0);
        break;
      case "a":
        this.moveChar(0, -1);
        break;
      case "d":
        this.moveChar(0, 1);
        break;
      case "r":
        this.resetLevel();
        break;
    }
  }

  //Convertir el estado actual del nivel en componentes para visualizar
  game2React() {
    let content = [];
    let game = [];
    //Add level tiles
    for (let i = 0; i < this.state.level.length; i++) {
      content = [];
      for (let j = 0; j < this.state.level[i].length; j++) {
        content.push(
          <Square value={this.state.level[i][j]} key={i + "-" + j} />
        );
      }
      game.push(
        <div className="row" key={i}>
          {content}
        </div>
      );
    }
    //Add player and boxes
    game.push(
      <Entity
        value={"player"}
        posx={this.state.pos[0]}
        posy={this.state.pos[1]}
        key={"player"}
      />
    );
    for (let i = 0; i < this.state.boxes.length / 2; i++) {
      if (
        this.state.level[this.state.boxes[i * 2]][
          this.state.boxes[i * 2 + 1]
        ] == 3
      ) {
        game.push(
          <Entity
            value={"boxgoal"}
            posx={this.state.boxes[i * 2]}
            posy={this.state.boxes[i * 2 + 1]}
            key={"box - " + i}
          />
        );
      } else {
        game.push(
          <Entity
            value={"box"}
            posx={this.state.boxes[i * 2]}
            posy={this.state.boxes[i * 2 + 1]}
            key={"box - " + i}
          />
        );
      }
    }
    //Add level complete message
    var complete = "";
    if (this.state.complete) {
      complete = "LEVEL COMPLETE";
    }
    game.push(
      <div className="controls" key="complete">
        <div className="reset" onClick={() => this.resetLevel()}>
          <svg viewBox="0 0 512 512">
            <path
              transform="scale(-1, 1) translate(-500, 0)"
              d="M256.455 8c66.269.119 126.437 26.233 170.859 68.685l35.715-35.715C478.149 25.851 504 36.559 504 57.941V192c0 13.255-10.745 24-24 24H345.941c-21.382 0-32.09-25.851-16.971-40.971l41.75-41.75c-30.864-28.899-70.801-44.907-113.23-45.273-92.398-.798-170.283 73.977-169.484 169.442C88.764 348.009 162.184 424 256 424c41.127 0 79.997-14.678 110.629-41.556 4.743-4.161 11.906-3.908 16.368.553l39.662 39.662c4.872 4.872 4.631 12.815-.482 17.433C378.202 479.813 319.926 504 256 504 119.034 504 8.001 392.967 8 256.002 7.999 119.193 119.646 7.755 256.455 8z"
            />
          </svg>
        </div>
        <div className="complete">{complete}</div>
      </div>
    );
    return game;
  }

  render() {
    const game = this.game2React();
    return <div className="gameboard">{game}</div>;
  }
}

export default GameBoard;
