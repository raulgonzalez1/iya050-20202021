import React, { useState } from "react";
import GameBoard from "./GameBoard.jsx";
import levels from "../../assets/levels.json";

function LevelSelector() {
  const [level, setLevel] = useState("0");
  const [style, setStyle] = useState("menu");
  const [menuStatus, setMenuStatus] = useState("open");

  //Control del estado del menu
  const handleClick = () => {
    switch (menuStatus) {
      case "open":
        setMenuStatus("close");
        setStyle("menu active");
        break;
      case "close":
        setMenuStatus("open");
        setStyle("menu");
        break;
    }
  };

  //Rellenar menu con un boton por nivel
  const fillButtons = () => {
    let content = [];
    for (let k in levels) {
      content.push(
        <button onClick={() => setLevel(k)} key={k}>
          LEVEL {k.toUpperCase()}
        </button>
      );
    }
    return content;
  };

  const content = fillButtons();
  return (
    <div>
      <div className="menubutton" onClick={handleClick}>
        <div className="line" />
        <div className="line" />
        <div className="line" />
      </div>
      <div className={style}>{content}</div>
      <div className="center">
        <GameBoard level={level} />
      </div>
    </div>
  );
}
export default LevelSelector;
