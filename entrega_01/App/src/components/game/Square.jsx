import React from "react";

//Componente base del tablero, comprueba el tipo para renderizar la imagen adecuada
class Square extends React.Component {
  constructor(props) {
    super(props);
  }

  squareType() {
    let typenumber = this.props.value;
    let type = "";
    switch (typenumber) {
      case 1:
        type = "wall";
        break;
      case 2:
        type = "floor";
        break;
      case 3:
        type = "goal";
    }
    return type;
  }

  render() {
    const style = "square img " + this.squareType();

    return <div className={style}></div>;
  }
}

export default Square;
