import React from "react";

//Entidades del juego (Jugador y cajas)
class Entity extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    let x = this.props.posx * 40;
    let y = this.props.posy * 40;
    let classname = this.props.value + " square img";
    return (
      <div
        className={classname}
        style={{ position: "absolute", top: x, left: y }}
      ></div>
    );
  }
}

export default Entity;
