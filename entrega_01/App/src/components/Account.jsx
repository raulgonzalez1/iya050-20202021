import React from "react";
import LogOut from "./authenticator/LogOut.jsx";
import LevelSelector from "./game/LevelSelector.jsx";

class Account extends React.Component {
  render() {
    return (
      <div>
        <LogOut />
        <div>
          <LevelSelector />
        </div>
      </div>
    );
  }
}

export default Account;
