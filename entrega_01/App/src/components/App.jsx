import React from "react";
import LogIn from "./authenticator/LogIn.jsx";
import Account from "./Account.jsx";
import Register from "./authenticator/Register.jsx";
import { Route, Switch } from "react-router-dom";

class App extends React.Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={LogIn} />
        <Route path="/register" component={Register} />
        <Route path="/account" component={Account} />
      </Switch>
    );
  }
}

export default App;
