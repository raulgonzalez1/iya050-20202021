import React from "react";

function Toast(props) {
  return (
    <div className="toast">
      <div>{props.text}</div>
    </div>
  );
}

export default Toast;
