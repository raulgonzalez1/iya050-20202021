import React from "react";
import { Redirect } from "react-router-dom";
import Toast from "../toast/Toast.jsx";

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      name: "",
      password: "",
      isReg: false,
      error: false,
    };
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleUsernameChange = this.handleUsernameChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
  }

  //Control del componente Toast
  componentDidUpdate() {
    if (this.state.error) {
      setTimeout(() => {
        this.removeError();
      }, 5000);
    }
  }
  removeError() {
    this.setState({ error: false });
  }

  handleNameChange(e) {
    this.setState({ name: e.target.value });
  }
  handleUsernameChange(e) {
    this.setState({ username: e.target.value });
  }
  handlePasswordChange(e) {
    this.setState({ password: e.target.value });
  }

  //Volver a vista logIn
  handleGoBack(e) {
    this.setState({ isReg: true });
  }

  //Fetch para registro de nuevo usuario
  handleClick(e) {
    const u = this.state.username;
    const p = this.state.password;
    const n = this.state.name;
    const url =
      "https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/users/" +
      u;
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ firstName: n, password: p }),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.hasOwnProperty("firstName")) this.setState({ isReg: true });
        else {
          this.setState({ error: true });
        }
      });
  }

  render() {
    const username = this.state.username;
    const password = this.state.password;
    const name = this.state.name;
    const isReg = this.state.isReg;
    var redirect = "";
    if (isReg) {
      redirect = <Redirect to="/" />;
    }
    var error = "";
    if (this.state.error) {
      error = <Toast text="Username already in use" />;
    }
    return (
      <div>
        <button
          className="topLeft"
          onClick={(i) => this.handleGoBack(i)}
          type="submit"
        >
          Go Back
        </button>
        <div className="center">
          <div className="authcontainer">
            <div className="authfield">
              <label className="authlabel">Name</label>
              <input
                value={name}
                onChange={this.handleNameChange}
                type="text"
                placeholder="Enter Name"
              />
            </div>
            <div className="authfield">
              <label className="authlabel">Username</label>
              <input
                value={username}
                onChange={this.handleUsernameChange}
                type="text"
                placeholder="Enter Username"
              />
            </div>
            <div className="authfield">
              <label className="authlabel">Password</label>
              <input
                value={password}
                onChange={this.handlePasswordChange}
                type="password"
                placeholder="Enter Password"
              />
            </div>
            <div className="authfield">
              <button onClick={(i) => this.handleClick(i)} type="submit">
                Sign Up
              </button>
            </div>
            {redirect}
          </div>
        </div>
        {error}
      </div>
    );
  }
}

export default Register;
