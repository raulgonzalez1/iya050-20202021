import React from "react";
import { Link, Redirect } from "react-router-dom";
import Toast from "../toast/Toast.jsx";

class LogIn extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      isLogIn: this.checkLogIn(),
      error: false,
    };
    this.handleUsernameChange = this.handleUsernameChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
  }

  //Comprobar si existe un token de sesion
  checkLogIn() {
    if (localStorage.hasOwnProperty("sessionToken")) {
      return true;
    }
    return false;
  }

  //Control del componente Toast
  componentDidUpdate() {
    if (this.state.error) {
      setTimeout(() => {
        this.removeError();
      }, 5000);
    }
  }
  removeError() {
    this.setState({ error: false });
  }

  handleUsernameChange(e) {
    this.setState({ username: e.target.value });
  }
  handlePasswordChange(e) {
    this.setState({ password: e.target.value });
  }

  //Fetch para loguear al usuario
  handleClick(e) {
    const u = this.state.username;
    const p = this.state.password;
    const url =
      "https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/users/" +
      u +
      "/sessions";
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ password: p }),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.hasOwnProperty("sessionToken")) {
          localStorage.setItem("sessionToken", data.sessionToken);
          this.setState({ isLogIn: true });
        } else {
          this.setState({ error: true });
        }
      });
  }

  render() {
    var redirect = "";
    if (this.state.isLogIn) {
      redirect = <Redirect to="/account" />;
    }
    var error = "";
    if (this.state.error) {
      error = <Toast text="Wrong username or password" />;
    }
    return (
      <div>
        <div className="center">
          <div className="authcontainer">
            <div className="authfield">
              <label className="authlabel">Username</label>
              <input
                value={this.state.username}
                onChange={this.handleUsernameChange}
                type="text"
                placeholder="Enter Username"
              />
            </div>
            <div className="authfield">
              <label className="authlabel">Password</label>
              <input
                value={this.state.password}
                onChange={this.handlePasswordChange}
                type="password"
                placeholder="Enter Password"
              />
            </div>
            <div className="authfield">
              <button onClick={(i) => this.handleClick(i)} type="submit">
                LOGIN
              </button>
            </div>
            <div className="authfield">
              <label className="authlabel">Don't have an account?</label>
              <Link to="/register">
                <button className="signup">Sign Up</button>
              </Link>
            </div>
            {redirect}
          </div>
        </div>
        {error}
      </div>
    );
  }
}

export default LogIn;
