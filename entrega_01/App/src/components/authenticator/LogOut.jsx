import React from "react";
import { Link } from "react-router-dom";

class LogOut extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
    };
  }

  //Fetch para recibir los datos del usuario en base a un token de login
  componentDidMount() {
    const t = localStorage.getItem("sessionToken");
    const url =
      "https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/user";
    fetch(url, {
      method: "GET",
      headers: {
        Authorization: t,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.hasOwnProperty("firstName")) {
          this.setState({ name: data.firstName });
        }
      });
  }

  //Fetch para desloguear al usuario
  handleClick(e) {
    const t = localStorage.getItem("sessionToken");
    const url =
      "https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/sessions/" +
      t;
    fetch(url, {
      method: "DELETE",
      headers: {
        Authorization: t,
      },
    }).then(localStorage.removeItem("sessionToken"));
  }

  render() {
    return (
      <div className="topRight">
        <div className="user">
          <div className="player square img" />
          <h2>{this.state.name}</h2>
        </div>
        <Link to="/">
          <button onClick={(i) => this.handleClick(i)} type="submit">
            LogOut
          </button>
        </Link>
      </div>
    );
  }
}

export default LogOut;
